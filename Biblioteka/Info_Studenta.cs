﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Biblioteka
{
    public partial class Info_Studenta : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=RELJIC-PC\SQLEXPRESS;Initial Catalog=biblioteka;Integrated Security=True;Pooling=False");
        public Info_Studenta()
        

           
        {
            InitializeComponent();
        }
        String pwd = Class1.GetRandomPassword(20);
        String wanted_path;
        DialogResult result;
        private void Info_Studenta_Load(object sender, EventArgs e)
        {
            int i = 0;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();

           
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from student_info";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            Bitmap img;
            DataGridViewImageColumn imageCol = new DataGridViewImageColumn();
            imageCol.Width = 500;
            imageCol.HeaderText = "slike studenata";
            imageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            imageCol.Width = 100;
            dataGridView1.Columns.Add(imageCol);
            foreach(DataRow dr in dt.Rows)
            {
                img = new Bitmap(@"..\..\" + dr["slika_studenta"].ToString());
                dataGridView1.Rows[i].Cells[8].Value = img;
                dataGridView1.Rows[i].Height = 100;
                i = i + 1;
            }

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                dataGridView1.Columns.Clear();
                dataGridView1.Refresh();
                int i = 0;
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from student_info where ime_studenta like('%"+textBox1.Text+"%') ";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                Bitmap img;
                DataGridViewImageColumn imageCol = new DataGridViewImageColumn();
                imageCol.Width = 500;
                imageCol.HeaderText = "slike studenata";
                imageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
                imageCol.Width = 100;
                dataGridView1.Columns.Add(imageCol);
                foreach (DataRow dr in dt.Rows)
                {
                    img = new Bitmap(@"..\..\" + dr["slika_studenta"].ToString());
                    dataGridView1.Rows[i].Cells[8].Value = img;
                    dataGridView1.Rows[i].Height = 100;
                    i = i + 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from student_info where id="+i+"";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach(DataRow dr in dt.Rows)
            {
                ime_studenta.Text = dr["ime_studenta"].ToString();
                broj_upisa_studenata.Text = dr["broj_upisa_studenta"].ToString();
                odeljenje_studenta.Text = dr["odeljenje_studenta"].ToString();
                sem_studenta.Text = dr["sem_studenta"].ToString();
                kontakt.Text = dr["kontakt"].ToString();
                email.Text = dr["email"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            result = openFileDialog1.ShowDialog();
            openFileDialog1.Filter = "JPEG Files(*.jpeg)|*.jpeg|PNG Files(*.png)|*.png|JPG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif";
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(result == DialogResult.OK)
            {
                int i;
                i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
                string img_path;
                File.Copy(openFileDialog1.FileName, wanted_path + "\\slike studenata\\" + pwd + ".jpg");
                img_path = "\\slike studenata\\" + pwd + ".jpg";
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update student_info set ime_studenta='"+ime_studenta.Text+"',slika_studenta='"+img_path.ToString()+"',broj_upisa_studenta='"+broj_upisa_studenata.Text+"',odeljenje_studenta='"+odeljenje_studenta.Text+"',sem_studenta='"+sem_studenta.Text+"',kontakt='"+kontakt.Text+"',email='"+email.Text+"'where id=" + i + "";
                cmd.ExecuteNonQuery();
                MessageBox.Show("Unos je sacuvan uspesno");

            }
            else if(result == DialogResult.Cancel)
            {
                int i;
                i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update student_info set ime_studenta='" + ime_studenta.Text + "',broj_upisa_studenta='" + broj_upisa_studenata.Text + "',odeljenje_studenta='" + odeljenje_studenta.Text + "',sem_studenta='" + sem_studenta.Text + "',kontakt='" + kontakt.Text + "',email='" + email.Text + "'where id=" + i + "";
                cmd.ExecuteNonQuery();
                MessageBox.Show("Unos je sacuvan uspesno");
            }
        }
    }
}
