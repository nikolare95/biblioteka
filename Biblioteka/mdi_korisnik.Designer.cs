﻿namespace Biblioteka
{
    partial class mdi_korisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.knjigaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajNovuKnjiguToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledKnjigaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajStudentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoStudentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.izdajaKnjigaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.knjigaToolStripMenuItem,
            this.studentToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(632, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // knjigaToolStripMenuItem
            // 
            this.knjigaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajNovuKnjiguToolStripMenuItem,
            this.pregledKnjigaToolStripMenuItem,
            this.izdajaKnjigaToolStripMenuItem});
            this.knjigaToolStripMenuItem.Name = "knjigaToolStripMenuItem";
            this.knjigaToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.knjigaToolStripMenuItem.Text = "Knjiga";
            // 
            // dodajNovuKnjiguToolStripMenuItem
            // 
            this.dodajNovuKnjiguToolStripMenuItem.Name = "dodajNovuKnjiguToolStripMenuItem";
            this.dodajNovuKnjiguToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dodajNovuKnjiguToolStripMenuItem.Text = "Dodaj novu knjigu";
            this.dodajNovuKnjiguToolStripMenuItem.Click += new System.EventHandler(this.dodajNovuKnjiguToolStripMenuItem_Click);
            // 
            // pregledKnjigaToolStripMenuItem
            // 
            this.pregledKnjigaToolStripMenuItem.Name = "pregledKnjigaToolStripMenuItem";
            this.pregledKnjigaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pregledKnjigaToolStripMenuItem.Text = "Pregled Knjiga";
            this.pregledKnjigaToolStripMenuItem.Click += new System.EventHandler(this.pregledKnjigaToolStripMenuItem_Click);
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajStudentaToolStripMenuItem,
            this.infoStudentaToolStripMenuItem});
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.studentToolStripMenuItem.Text = "Student";
            // 
            // dodajStudentaToolStripMenuItem
            // 
            this.dodajStudentaToolStripMenuItem.Name = "dodajStudentaToolStripMenuItem";
            this.dodajStudentaToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.dodajStudentaToolStripMenuItem.Text = "Dodaj studenta";
            this.dodajStudentaToolStripMenuItem.Click += new System.EventHandler(this.dodajStudentaToolStripMenuItem_Click);
            // 
            // infoStudentaToolStripMenuItem
            // 
            this.infoStudentaToolStripMenuItem.Name = "infoStudentaToolStripMenuItem";
            this.infoStudentaToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.infoStudentaToolStripMenuItem.Text = "Info studenta";
            this.infoStudentaToolStripMenuItem.Click += new System.EventHandler(this.infoStudentaToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(632, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // izdajaKnjigaToolStripMenuItem
            // 
            this.izdajaKnjigaToolStripMenuItem.Name = "izdajaKnjigaToolStripMenuItem";
            this.izdajaKnjigaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.izdajaKnjigaToolStripMenuItem.Text = "Izdaja Knjiga";
            this.izdajaKnjigaToolStripMenuItem.Click += new System.EventHandler(this.izdajaKnjigaToolStripMenuItem_Click);
            // 
            // mdi_korisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "mdi_korisnik";
            this.Text = "mdi_korisnik";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem knjigaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajNovuKnjiguToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledKnjigaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajStudentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoStudentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izdajaKnjigaToolStripMenuItem;
    }
}



