﻿namespace Biblioteka
{
    partial class Izdaja_knjige
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_upisa = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_imestudenta = new System.Windows.Forms.TextBox();
            this.txt_odeljenja = new System.Windows.Forms.TextBox();
            this.txt_sem = new System.Windows.Forms.TextBox();
            this.txt_kontakt = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.txt_imeknjige = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listBox1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_imeknjige);
            this.panel1.Controls.Add(this.txt_email);
            this.panel1.Controls.Add(this.txt_kontakt);
            this.panel1.Controls.Add(this.txt_sem);
            this.panel1.Controls.Add(this.txt_odeljenja);
            this.panel1.Controls.Add(this.txt_imestudenta);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txt_upisa);
            this.panel1.Location = new System.Drawing.Point(89, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 509);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txt_upisa
            // 
            this.txt_upisa.Location = new System.Drawing.Point(22, 25);
            this.txt_upisa.Name = "txt_upisa";
            this.txt_upisa.Size = new System.Drawing.Size(165, 20);
            this.txt_upisa.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(40, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 34);
            this.button1.TabIndex = 1;
            this.button1.Text = "Pretrazi ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_imestudenta
            // 
            this.txt_imestudenta.Location = new System.Drawing.Point(403, 22);
            this.txt_imestudenta.Name = "txt_imestudenta";
            this.txt_imestudenta.Size = new System.Drawing.Size(200, 20);
            this.txt_imestudenta.TabIndex = 2;
            // 
            // txt_odeljenja
            // 
            this.txt_odeljenja.Location = new System.Drawing.Point(403, 62);
            this.txt_odeljenja.Name = "txt_odeljenja";
            this.txt_odeljenja.Size = new System.Drawing.Size(200, 20);
            this.txt_odeljenja.TabIndex = 3;
            // 
            // txt_sem
            // 
            this.txt_sem.Location = new System.Drawing.Point(403, 112);
            this.txt_sem.Name = "txt_sem";
            this.txt_sem.Size = new System.Drawing.Size(200, 20);
            this.txt_sem.TabIndex = 4;
            // 
            // txt_kontakt
            // 
            this.txt_kontakt.Location = new System.Drawing.Point(403, 166);
            this.txt_kontakt.Name = "txt_kontakt";
            this.txt_kontakt.Size = new System.Drawing.Size(200, 20);
            this.txt_kontakt.TabIndex = 5;
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(403, 217);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(200, 20);
            this.txt_email.TabIndex = 6;
            // 
            // txt_imeknjige
            // 
            this.txt_imeknjige.Location = new System.Drawing.Point(403, 296);
            this.txt_imeknjige.Name = "txt_imeknjige";
            this.txt_imeknjige.Size = new System.Drawing.Size(200, 20);
            this.txt_imeknjige.TabIndex = 7;
            this.txt_imeknjige.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_imeknjige_KeyDown);
            this.txt_imeknjige.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_imeknjige_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(311, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ime studenta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Odeljenje studenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(334, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Sem";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(325, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Kontakt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(334, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(334, 296);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ime knjige";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Unesi broj upisa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(315, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Datume izdaje";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(403, 249);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 16;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(216, 450);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 32);
            this.button2.TabIndex = 17;
            this.button2.Text = "Izdaj knjigu";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(403, 317);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(200, 108);
            this.listBox1.TabIndex = 18;
            this.listBox1.Visible = false;
            this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
            this.listBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyDown);
            // 
            // Izdaja_knjige
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 584);
            this.Controls.Add(this.panel1);
            this.Name = "Izdaja_knjige";
            this.Text = "Izdaja_knjige";
            this.Load += new System.EventHandler(this.Izdaja_knjige_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_imeknjige;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_kontakt;
        private System.Windows.Forms.TextBox txt_sem;
        private System.Windows.Forms.TextBox txt_odeljenja;
        private System.Windows.Forms.TextBox txt_imestudenta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_upisa;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox1;
    }
}