﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Biblioteka
{
    public partial class Izdaja_knjige : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=RELJIC-PC\SQLEXPRESS;Initial Catalog=biblioteka;Integrated Security=True;Pooling=False");

        public Izdaja_knjige()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from student_info where broj_upisa_studenta ='"+txt_upisa.Text+"'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            i =Convert.ToInt32(dt.Rows.Count.ToString());

            if (i ==0)
            {
                MessageBox.Show("Datum ovog upisa nije pronadjen");
            }
            else
            {

            }
            foreach(DataRow dr in dt.Rows)
            {
                txt_imestudenta.Text = dr["ime_studenta"].ToString();
                txt_odeljenja.Text = dr["odeljenje_studenta"].ToString();
                txt_sem.Text = dr["sem_studenta"].ToString();
                txt_kontakt.Text = dr["kontakt"].ToString();
                txt_email.Text = dr["email"].ToString();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Izdaja_knjige_Load(object sender, EventArgs e)
        {
            if(con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
        }

        private void txt_imeknjige_KeyUp(object sender, KeyEventArgs e)
        {
            int count = 0;
            if (e.KeyCode !=Keys.Enter)
            {
                listBox1.Items.Clear();

                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from info_knjige where ime_knjige like('%"+txt_imeknjige.Text+"%')";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                count = Convert.ToInt32(dt.Rows.Count.ToString());

                if(count>0)
                {
                    listBox1.Visible = true;
                    foreach(DataRow dr in dt.Rows)
                    {
                        listBox1.Items.Add(dr["ime_knjige"].ToString());
                    }
                }
            }
        }

        private void txt_imeknjige_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==Keys.Down)
            {
                listBox1.Focus();
                listBox1.SelectedIndex = 0;
            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==Keys.Enter)
            {
                txt_imeknjige.Text = listBox1.SelectedItem.ToString();
                listBox1.Visible = false;
            }
        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            txt_imeknjige.Text = listBox1.SelectedItem.ToString();
            listBox1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into Izdaja_knjige values('"+txt_upisa.Text+"','"+txt_imestudenta.Text+"','"+txt_odeljenja.Text+"','"+txt_sem.Text+"','"+txt_kontakt.Text+"','"+txt_email.Text+"','"+txt_imeknjige.Text+"','"+dateTimePicker1.Value.ToShortDateString()+"')";
            cmd.ExecuteNonQuery();
            MessageBox.Show("Knjiga je izdata uspesno");
        }
    }
}
