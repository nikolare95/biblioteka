﻿namespace Biblioteka
{
    partial class Info_Studenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ime_studenta = new System.Windows.Forms.TextBox();
            this.broj_upisa_studenata = new System.Windows.Forms.TextBox();
            this.odeljenje_studenta = new System.Windows.Forms.TextBox();
            this.sem_studenta = new System.Windows.Forms.TextBox();
            this.kontakt = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(214, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(627, 202);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(22, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(186, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.email);
            this.panel1.Controls.Add(this.kontakt);
            this.panel1.Controls.Add(this.sem_studenta);
            this.panel1.Controls.Add(this.odeljenje_studenta);
            this.panel1.Controls.Add(this.broj_upisa_studenata);
            this.panel1.Controls.Add(this.ime_studenta);
            this.panel1.Location = new System.Drawing.Point(214, 231);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 215);
            this.panel1.TabIndex = 2;
            // 
            // ime_studenta
            // 
            this.ime_studenta.Location = new System.Drawing.Point(118, 19);
            this.ime_studenta.Name = "ime_studenta";
            this.ime_studenta.Size = new System.Drawing.Size(98, 20);
            this.ime_studenta.TabIndex = 0;
            // 
            // broj_upisa_studenata
            // 
            this.broj_upisa_studenata.Location = new System.Drawing.Point(324, 19);
            this.broj_upisa_studenata.Name = "broj_upisa_studenata";
            this.broj_upisa_studenata.Size = new System.Drawing.Size(90, 20);
            this.broj_upisa_studenata.TabIndex = 1;
            // 
            // odeljenje_studenta
            // 
            this.odeljenje_studenta.Location = new System.Drawing.Point(521, 19);
            this.odeljenje_studenta.Name = "odeljenje_studenta";
            this.odeljenje_studenta.Size = new System.Drawing.Size(84, 20);
            this.odeljenje_studenta.TabIndex = 2;
            // 
            // sem_studenta
            // 
            this.sem_studenta.Location = new System.Drawing.Point(118, 76);
            this.sem_studenta.Name = "sem_studenta";
            this.sem_studenta.Size = new System.Drawing.Size(98, 20);
            this.sem_studenta.TabIndex = 3;
            // 
            // kontakt
            // 
            this.kontakt.Location = new System.Drawing.Point(324, 76);
            this.kontakt.Name = "kontakt";
            this.kontakt.Size = new System.Drawing.Size(90, 20);
            this.kontakt.TabIndex = 4;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(521, 76);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(84, 20);
            this.email.TabIndex = 5;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(118, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "Izaberi fajl";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(324, 136);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 25);
            this.button2.TabIndex = 7;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ime studenta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Broj upisa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(451, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Odeljenje";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Sem";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(264, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "kontakt";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(454, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "email";
            // 
            // Info_Studenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 517);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Info_Studenta";
            this.Text = "Info_Studenta";
            this.Load += new System.EventHandler(this.Info_Studenta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox kontakt;
        private System.Windows.Forms.TextBox sem_studenta;
        private System.Windows.Forms.TextBox odeljenje_studenta;
        private System.Windows.Forms.TextBox broj_upisa_studenata;
        private System.Windows.Forms.TextBox ime_studenta;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}